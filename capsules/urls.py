from django.urls import path
from . import views


app_name = 'capsulecoprs'
urlpatterns = [
    path('', views.index, name='index'),
    path('capsule/<str:id>', views.capsule, name='capsule'),
]