from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
import requests

from capsules.models import Capsule

#Vue - Lister capsules
def index(request):
    #Vérifie si la base de données SQLite contient déjà les données.
    #Si oui, affiche ces données
    data = Capsule.objects.all()
    if data.count:
        context = {
            'entries': data,
        }
        return render(request, 'index.html', context)

    #Sinon, lance une requête HTTP pour récupérer les données
    r = requests.get('https://api.spacexdata.com/v4/capsules')
    if r.status_code == 200:
        data = r.json()
        #Enregistre les données récupérées dans... 
        #la base de données avant de les affichées sur la page
        for x in data:
            capsule = Capsule()
            capsule.jsonToClass(x)
            capsule.save()
        context = {
            'entries': data,
        }
        return render(request, 'index.html', context)
    return HttpResponse('Could load save data, Please Retry')

#Vue - Voir details capsule
def capsule(request, id):
    capsule = Capsule.objects.get(id=id)
    template = loader.get_template('capsule.html')
    context = {
        'data': capsule,
    }
    return HttpResponse(template.render(context, request))