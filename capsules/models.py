from django.db import models

#Model Capsule
class Capsule(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    reuse_count = models.IntegerField()
    water_landings = models.IntegerField()
    land_landings = models.IntegerField()
    last_update = models.CharField(max_length=255, blank=True, null=True)
    serial = models.CharField(max_length=50, blank=True, null=True)
    status = models.CharField(max_length=50, blank=True, null=True)
    type = models.CharField(max_length=50, blank=True, null=True)



    # Fonction pour deserialiser les données json en model Capsule
    def jsonToClass(self, aux):
        self.id = aux['id']
        self.reuse_count = aux['reuse_count']
        self.water_landings = aux['water_landings']
        self.land_landings = aux['land_landings']
        self.last_update = aux['last_update']
        self.serial = aux['serial']
        self.status = aux['status']
        self.type = aux['type']
