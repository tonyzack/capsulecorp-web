## Note

J'ai pris la liberté de deployer le projet en live
Lien : [https://nadhholy.me/spacex](https://nadhholy.me/spacex)

# Versions utilisées
    -Python: 3.8.9
    -Django: 4.1.1
    -pip: 22.2.2

# librairies et modules utilisés

## Requests
    [](https://github.com/psf/requests)
    $ python -m pip install requests


## Bootstrap 5 
    [](https://getbootstrap.com/)


## Soft UI Dashboard
    [](https://www.creative-tim.com/learning-lab/bootstrap/overview/soft-ui-dashboard)

